const mongoose = require('mongoose')

var CloudSchema = mongoose.Schema({
    name: String,
    items: [{ type: mongoose.Schema.ObjectId, ref: 'Note' }],
    nItems: Number,
    color: String
}, {
    timestamps: true
})

/* METHODS */

/*
 * create a cloud with given name
 */

CloudSchema.statics.create = function( name ) {
  return new Promise(( resolve, reject ) => {
    
    if(name === '')
      return reject({ message: 'name cannot be empty' })
    else if(1 === 0) // other validation!
      return reject({ message: 'invalid name' })
      
    // Create a cloud
    const cloud = new this({ name: name // || "Noname Cloud"
    })
    // Save cloud in the database
    cloud.save()
      .then(cld  => { resolve(cld) })
      .catch(err => { reject(err)  })
  })
}


/*
 * remove cloud, if it's not empty
 */

CloudSchema.statics.removeId = function( cloudId ) {
  return new Promise(( resolve, reject ) => {
    
    if(cloudId === '')
      return reject({ message: 'no cloudId given' })
      
    // find and remove cloud
    this.findById(cloudId)
      .then(cld  => {
        // check if cloud is empty
        if(cld.items.length > 0 )
          return reject({ message: 'cloud is not empty' })
      
        cld.remove()
          .then(() => { resolve(cld) })
          .catch(err => { reject(err)  })
      })
      .catch(err => { reject({ message: 'could not find cloud with id ' + cloudId })  })
  })
}


/*
 * insert a note
 */

CloudSchema.statics.insertNote = function( cloudId, noteId ) {
  return new Promise(( resolve, reject ) => {
    console.log('insert note ' + noteId + ' to cloud ' + cloudId)
    
    this.findById(cloudId).then(cld => {
      //console.log('>> > ' + cld.items.length)
      
        // push note to items and update count
      cld.items.push(noteId)
      cld.nItems = cld.items.length
      cld.save().then(c => {
        //console.log('>> > inserted')
        resolve(c)
      }).catch(err => reject(err))
    })
  })
}

/*
 * get all clouds sorted
 */

CloudSchema.statics.getAllSorted = function( sortBy ) {
  return new Promise(( resolve, reject ) => {
    
    this.find().sort( sortBy ).exec(( err, clouds ) => {
      if( err ) { reject( err ) }
      else { resolve( clouds ) }
    })
  })
}

/*
 * e.g.
 * Cloud.getAllSortedPopulated({ nItems: -1 }, { rating: -1 }).then(clouds => {...})
 */

CloudSchema.statics.getAllSortedPopulated = function(sortCloudsBy, sortNotesBy) {
  return new Promise((resolve, reject) => {
    
    this.find().sort(sortCloudsBy)
      .populate({
          path: 'items'
        , options: { sort: sortNotesBy}
      })
      .exec((err, clouds) => {
        if(err) {
          reject(err)
        }
        else {
          resolve(clouds)
        }
      })
  })
}

/*
 * e.g.
 * Cloud.getOnePopulatedSorted({ rating: -1 }).then(notes => {...})
 */

CloudSchema.statics.getOnePopulatedSorted = function(cloudId, sortNotesBy) {
  return new Promise((resolve, reject) => {
    
    this.findById(cloudId)
      .populate({
          path: 'items'
        , options: { sort: sortNotesBy}
      })
      .exec((err, clouds) => {
        if(err) { reject(err) }
        else    { resolve(clouds) }
      })
    
    // old code: note.find({ '_id': { $in: c.items }}).sort({ 'rating': -1 }).then(notes => {
    
  })
}

/*
 * move notes from one cloud to another
 */

CloudSchema.statics.moveNotes = function(cloudFromId, cloudToId, noteIds) {
  return new Promise((resolve, reject) => {
    
    // 1. - check if origin is same as destination
    //  X - check if all IDs are found in cloudFrom
    // 2. put these in cloudTo
    // 3. remove those from cloudFrom
    
    console.log('>> ' + cloudFromId)
    console.log('>> ' + cloudToId)
    
    if(cloudFromId == cloudToId) {
      console.log('! same cloud IDs')
      reject({ message: 'note is already in here' })
      return
    }
    
    var conditions = { '_id': cloudToId, 'items._id': { $ne: noteIds }}
    var update = {
      $addToSet: { items: noteIds },
      $inc: { nItems: noteIds.length }}
    
    this.update(conditions, update).then(c => {
      console.log('>> inserted: ' + JSON.stringify(c))
      
      // remove from cloudFrom, if set
      if(cloudFromId)
      {
        this.update({ _id: cloudFromId }, {
          $pullAll: { "items": noteIds },
          $inc: { nItems: -noteIds.length }}, function(err) {
            if(err) { reject(err) }
            else    { resolve() }
        })
      }
      else { return resolve() }
    }).catch(err => { reject(err) })
  })
}


module.exports = mongoose.model('Cloud', CloudSchema);
