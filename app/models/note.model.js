const mongoose = require('mongoose')

const NoteSchema = mongoose.Schema({
    title: String,
    content: String,
    cloudId: mongoose.Schema.Types.ObjectId,
    attr: {},
    rating: { type: Number, default: 0 }
}, {
    timestamps: true
})

  
/*
 * create a note with given content ( and title )
 */

NoteSchema.statics.create = function( content, title ) {
  return new Promise(( resolve, reject ) => {
    
    // Create a Note
    const note = new this({
      content: content,
      title: title || "Untitled Note"
    })

    // Save Note in the database
    note.save()
      .then(note => { resolve(note) })
      .catch(err => { reject(err)   })
  })
}

/*
 * search all notes' content and return matching ones
 */

NoteSchema.statics.searchContent = function search (contentQuery, cb) {
  return new Promise(( resolve, reject ) => {
    this.where('content', new RegExp(contentQuery, 'i'))
      .then(notes => resolve(notes))
      .catch(err => reject(err))
  })
}

/*
 * update title
 */

NoteSchema.methods.updateTitle = function( title ) {
  return new Promise(( resolve, reject ) => {
    
    this.title = title
    this.save()
      .then(note => { resolve(note) })
      .catch(err => { reject(err)   })
  })
};


module.exports = mongoose.model('Note', NoteSchema);