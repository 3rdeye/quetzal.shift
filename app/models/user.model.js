var mongoose = require('mongoose')
var hashish = require('../flow/helpers/hashish')


var UserSchema = mongoose.Schema({
  email: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  username: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  password: { type: String },
  passwordSalt: { type: String }
})


UserSchema.methods.setPassword = function(password) {
  // creating a unique salt for a particular user
  this.passwordSalt = hashish.salt()
  // hashing the password
  this.password = hashish.hash(password, this.passwordSalt)
}

UserSchema.methods.validPassword = function(password) {
    var hash = hashish.hash(password, this.passwordSalt)
    return this.password === hash
}

//authenticate input against database
UserSchema.statics.authenticate = function (user, password, callback) {
  // lookup user by email or username?
  var query = (user.indexOf('@') == -1) ? { username: user } : { email: user }
  
  this.findOne(query)
    .exec(function (err, user) {
      if (err) {
        return callback(err)
      } else if (!user) {
        var e = new Error('User not found.');
        e.status = 401;
        return callback(e);
      }
      
      if (user.validPassword(password)) {
        return callback(null, user);
      } else {
        console.log('>>> INVALID LOGIN <<<')
        return callback();
      }
    })
}

// hash the password before saving it to the database
UserSchema.pre('save', function (next) {
  this.setPassword(this.password)
  next()
})

module.exports = mongoose.model('User', UserSchema);
//var User = mongoose.model('User', UserSchema)
//module.exports = User