const responder = require('./helpers/responder.js')
const logger    = require('./helpers/logger.js')

const Note  = require('../models/note.model.js');
const Cloud = require('../models/cloud.model.js');

const patterns  = require('./wisdom/patterns.js')
const agent     = require('./agents/web.agent.js')


// -=-=-=-=-=-=- //

var replaceAll = function(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}



/* -=-=-=- */


// Create and Save a new Note
exports.create = (req, res) => {
  logger.in('note.create', req, res)
  
  // Validate request
  if (!responder.validate(res, { 'content': req.body.content })) return
  
  // -=-=-=- input correction -=-=-=- //
  
  // replace line breaks by html code
  var content = replaceAll(req.body.content, '\r\n', '<br>')
  
  // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //

  Note.create(content).then(note => {
    logger.put('note saved to db.')
    
    Note.find({content:'stuff'})
      .then(nt => {
        console.log('!!! got it')
        nt.updateTitle('ALLRIGHT')
         // .then(n => { console.log('/// ' + n.title)})
         // .catch(e => console.log('<<<ERR>>> ' + e))

    })
    
    //note.updateTitle('CHECK')

    // if cloudId is given, put note into cloud
    var cloudId = req.body.cloudId
    
    logger.put('cloudId: ' + cloudId)
    
    if(!cloudId) { return responder.okRedirect(res, '/')
    } else {
      var noteId = note._id
      logger.put('noteId: ' + noteId)

      // does not work with $inc - dont know why!!!

        // var conditions = { '_id': cloudId, 'items._id': { $ne: nId }}
        // var update = { $addToSet: { items: nId }, $inc: { nItems: 1 }}
        //   Cloud.update(conditions, update, function() {
        //   logger.put('>> > insert to cloud [' + cloudId + ']')
      console.log('0>>> ' + Object.getOwnPropertyNames(note))
      
      Cloud.insertNote(cloudId, noteId).then(() => {
        logger.put('...inserted to cloud.')
        // run recognition patterns and update note
        // ---> could be improved with only one db query in total
        var found = patterns.recognize(note)

        logger.put('recognition patterns found: ' + JSON.stringify(found))

        var fReturn = () => { responder.okRedirect(res, '/show?cloudId=' + cloudId) }

        // if youtube url is recognized, find the title
        if(found.hasOwnProperty('url-youtube')) {
          logger.put('find yt title...')
          agent.lookupPageTitle(note.content, function(title) {
            note.updateTitle(title)
              .then(()=>{ fReturn() })
              .catch(err => { responder.err(res, err) })
          })
        }
        else {
          fReturn()
        }
      }).catch(err => { return responder.err(res, err) })
    }
  }).catch(err => { return responder.err(res, err) })
}

// voting

exports.vote = (req, res) => {
  logger.in('vote', req, res)
  
  var noteId = req.query.noteId
  var voteUp = req.query.voteUp
  var cloudId = req.query.cloudId
  
  // validate inputs
  if(!responder.validate(res, {
    'noteId': noteId,
    'voteUp': voteUp
  })) return
  
  // numeric direction
  var vote = (voteUp === 'false') ? -1 : 1
  
  Note.findByIdAndUpdate(noteId, { $inc: { rating: vote }}, { new: true })
   .then(note => {
      // note not found?
      if(!note)
        return responder.errNotFound(res, 'could not find note ' + noteId)
      
      logger.put('note updated: ' + JSON.stringify(note))
      if(cloudId) {
        responder.okRedirect(res, '/show?cloudId=' + cloudId)
      } else {
        responder.okRedirect(res, '/clouds')
      }
    }).catch(err => {
      return responder.err(res, err)
    })
}


/*
 * print out database content as text
 */
exports.print = (req, res) => {
  logger.in('note.print', req, res)
  Note.find()
    .then(notes => {
      var strData = JSON.stringify(notes);

      // add line breaks for better readability
      strData = replaceAll(strData, ',"', ',<br>"');
      strData = replaceAll(strData, ',{', ',<br><br>{');

      return responder.okMsg(res, strData);
    }).catch(err => {
      return responder.err(res, err)
    });
}