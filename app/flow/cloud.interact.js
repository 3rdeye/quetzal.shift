const responder = require('./helpers/responder.js')
const logger    = require('./helpers/logger.js')

const moment = require('moment') // formatted dates

/* -=-=-=-=-=-=- */

const Cloud = require('../models/cloud.model.js')

/* -=-=-=-=-=-=- /*


create   :: req.body .cloudName
remove   :: req.query.id

overview :: 
show     :: req.query.cloudId

move     :: req.body .cloudFromId,
            req.body .cloudToId,
            req.body .noteIds
setColor :: req.query.cloudId
            req.body .color


/* -=-=-=-=-=-=- */

/*
 * Create a new Cloud
 */

exports.create = (req, res) => {
  logger.in('cloud.create', req, res)
  
  var name = req.body.cloudName
  
  // Validate request
  if (!responder.validate(res, { 'Cloud name': name })) return

  // Create a cloud
  Cloud.create(name).then(c => {
    responder.okRedirect(res, '/show?cloudId=' + c.id)
  }).catch(err => {
    responder.err(res, err)
  })
}

/*
 * Delete a cloud with the specified cloudId in the request
 */

exports.remove = (req, res) => {
  logger.in('cloud.remove', req, res)
  
  var cloudId = req.query.id;
  
  if(!responder.validate(res, {"cloudId":cloudId})) return
  
  logger.put('removing cloud, id: ' + cloudId)
  
  Cloud.removeId(cloudId)
    .then(()   => { responder.okRedirect(res, '/')})
    .catch(err => { responder.err(err) })
}


/*
 *  show all clouds and their notes
 *
 *  render: clouds (populated)
 */

exports.overview = (req, res) => {
  logger.in('cloud.showAll', req, res)
  
  Cloud.getAllSortedPopulated({ nItems: -1 }, { rating: -1 }).then(clouds => {
    if (!clouds)
      responder.err(res, null, 'error while fetching clouds')
    else {
      responder.ok(res, 'app', {
        vars: { clouds: clouds },
        view: 'allClouds',
        pagetitle: 'Quetzalito volando...!',
        moment: moment })
    }
  }).catch(err => { responder.err(res, err) })
}

/*
 * show one cloud
 */

exports.show = (req, res) => {
  logger.in('cloud.show', req, res)
  
  var cloudId = req.query.cloudId
  
  if(!responder.validate(res, { 'cloudId':cloudId })) return
  
    // first fetch the specific cloud in case the cloudId is not right (to not fetch all clouds for nothing...)
  Cloud.getOnePopulatedSorted(cloudId, { 'rating': -1, 'createdAt': 1 }).then(cloud => {
      // cloud id not found?
    if(!cloud) responder.errNotFound(res, 'no cloud found with id ' + cloudId)
    //logger.put('cloud: ' + cloud)
    Cloud.getAllSorted({ nItems: -1 }).then(clouds => {
      //logger.put('clouds: ' + clouds.length)

      responder.ok(res, 'app', {
        vars: {
          clouds: clouds,
          notes: cloud.items,
          cloudId: cloudId
        },
        view: 'oneCloud',
        pagetitle: 'Quetzalito ::: ' + cloud.name
      })
    }).catch(err => { responder.err(res, err) })
  
    // possible errors: ID does not exist
  }).catch(err => { responder.err(res, err) })
}


/*
 * move notes from one cloud to another
 *
 * [params] cloudFrom, cloudTo, noteIds[]
 *
 */

exports.move = (req, res) => {
  logger.in('cloud.move', req, res)
  
  // cloud IDs (from, to)
  var cloudFromId = req.body.cloudFrom;
  var cloudToId = req.body.cloudTo;
  
  // IDs of notes we want to move
  var noteIds = req.body.noteIds;
  
  if (!responder.validate(res, {
    'ID of cloud (origin)': cloudFromId,
    'IDs of notes': noteIds })) return
  
  // logging
  logger.put('>> move ' +noteIds.length+
              ' notes from cloud [' +cloudFromId+ ']' +
              ' to [' +cloudToId+ ']')
  
  Cloud.moveNotes(cloudFromId, cloudToId, noteIds)
    .then(() => { return responder.okRedirect(res, '/show?cloudId=' + cloudFromId) })
    .catch(err => responder.err(res, err) )
}


/*
 * set color of a cloud
 *
 *   cloudId
 *   color
 */

exports.setColor = (req, res) => {
  logger.in('cloud.setColor', req, res)
  
  var cloudId = req.query.cloudId
  var color = req.body.color
  
  if(!responder.validate(res, {"cloudId":cloudId, "color":color})) return
  
  logger.put('set color: ' + color)
  
  Cloud.findByIdAndUpdate(cloudId, { 
    $set: { color: color }}).then(c => {
      return responder.okRedirect(res, '/show?cloudId=' + cloudId)
  }).catch(err => {
    return responder.err(res, err)
  })
}
