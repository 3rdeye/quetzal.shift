var replaceAll = function(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}


exports.get = function(str) {
  return replaceAll(str, '/', '/<wbr>')  // get line breaks on urls
}
