const logger    = require('./logger.js')
const formatter = require('./formatter.js')

/* SUCCESS */

var checkRes = function(res) {
  //logger.put('>>> ' + (Object.getOwnPropertyNames(res)))
  if(typeof res !== 'object' || !res.connection)
    logger.put('<<< BUG >>> res not given! ')
}

exports.ok = (res, page, vars) => {
  checkRes(res)
  logger.out('render: ' + page)
  // return res.render(page, vars)
  if(vars)
    return res.render(page, Object.assign(vars, { formatter:formatter }))
  else
    return res.render(page)
}
exports.okRedirect = (res, page) => {
  checkRes(res)
  logger.out('redirect -> ' + page)
  return res.redirect(page)
}
exports.okMsg = (res, msg, path, pathname) => {
  checkRes(res)
  logger.out(msg)
  
  var pth, pthname
  
  pth = path ? path : '/'
  pthname = pathname ? pathname : 'home'
  
//   return res.send(msg + '<br><a href="/">back</a>')
  return res.render('notify', { vars: {
    message: msg,
    path: pth, pathName: pthname }})
}


/* ERRORS */

exports.err = (res, err, msg) => {
  if(err) {
    var code = 500
    var mssg = ''
    
    if(err.kind == 'ObjectId') {
      code = 404
      mssg = 'object with ID ' +err.value+ ' not found'
    }
    else {
      mssg = err.message
    }
    //return res.status(code).send({ message: mssg })
    return res.status(code).render('notify', { vars: {
      message: mssg,
      path: '/', pathName: 'home' }})
  } else {
    return res.status(500).send({ message: msg })
  }
}
exports.errNotFound = (res, msg) => {
  return res.status(404).send({ message: msg })
}
exports.errNotPossible = (res, msg) => {
  return res.status(400).send({ message: msg })
}

/* PARAMETER VALIDATION */

exports.validate = (res, vars) => {
  var wrong = (msg, code) => {
    logger.out(msg)
    res.status(code || 400).send({ message: msg })
    return false;
  }
  var bug = () => { return wrong('>> Server Bug! Do smthng!', 500) }
  
  //logger.put('typeof vars: ' + typeof vars)
  //logger.put('vars: ' + JSON.stringify(vars))
  
  if (!vars || typeof vars !== 'object') return bug()
  
  //logger.put('validate parameters: ' + JSON.stringify(vars))
  
  var checkVar = (key, val) =>
  {
    logger.put('validate parameter [' +key+ '] ' +val)
    if(!key) return bug()
    if(!val) return wrong('Missing input parameter: ' + key)
    return true
  }
  
  for (var v in vars) {
    if(!checkVar(v, vars[v])) return false
  }
  
  return true
}
