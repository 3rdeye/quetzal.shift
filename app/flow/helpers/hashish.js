var crypto = require('crypto')

// create salt
exports.salt = function() {
  return crypto.randomBytes(16).toString('hex')
}

// hashing phrase and salt with 1000 iterations, 64 length and sha512 digest
exports.hash = function(phrase, salt) {
  return crypto.pbkdf2Sync(phrase, salt, 
    1000, 64, `sha512`).toString(`hex`)
}