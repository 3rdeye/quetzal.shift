/*
 * LOGGING
 */


var log_t0 = null       // timestamp when entering log
var moduleName = ''     // name of current controller

  // start log
exports.in = function(name, req, res) {
  log_t0 = Date.now()
  moduleName = name //arguments.callee.toString() //name
  console.log('(logIn ' +moduleName+ ')')
  console.log('  req.query: ' + JSON.stringify(req.query))
  console.log('  req.body:  ' + JSON.stringify(req.body))
  console.log('\n')
}

  // log something
exports.put = function(msg) {
  console.log('(log   ' +moduleName+ ') ' + msg) }

  // end log
exports.out = function(msg) {
  var timeDif = Date.now() - log_t0
  console.log('(' +moduleName+ ' reply [' +
              timeDif+ 'ms]) ' + msg) }
