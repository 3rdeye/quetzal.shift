const logger    = require('../helpers/logger.js')

const Note      = require('../../models/note.model.js')


//// better to outsource note update !!!


/* note recognition patterns */

var patterns = [{
  name: 'url',
  startsWith: ['http://', 'https://']
},{
  name: 'url-youtube',
  startsWith: ['https://www.youtube.com/watch',
              'https://youtu.be/']
},{
  name: 'url-soundcloud',
  startsWith: ['https://soundcloud.com/']
}]

var extract = {
  'url-soundcloud': {
    name: 'id-soundcloud',
    domQuery: "meta[content^='soundcloud://sounds:']",
    extractAttr: 'content',
    extractAfter: 'soundcloud://sounds:'
  }
}

exports.getExtract = function(name) {
  return extract[name]
}

/* check one note for recognition patterns */
exports.recognize = (note) => {
  logger.put('run recognition...')
  var found = {}
  // iterate over all patterns
  for(p = 0; p < patterns.length; p++) {
    var pattern = patterns[p]
    
    //logger.put('pattern: ' +pattern.name+ ', startsWith: ' + JSON.stringify(pattern.startsWith))

    if((pattern.startsWith !== undefined) && (pattern.startsWith.length > 0)) {
      var content = note.content
      //logger.put('check note content >>> ' + content)
      for(j = 0; j < pattern.startsWith.length; j++) {
        // check if entry exists already
        if((!note.attr || !note.attr[pattern.name]) && content.startsWith(pattern.startsWith[j])) {
          logger.put('>>> found pattern [' +pattern.name+ '] \n  >> on note ['+note._id+'] ' + content)

          if(!note.attr)
            note.attr = {}

          // update note object
          note.attr[pattern.name] = content
          // update found list
          found[pattern.name] = content

          //logger.put('attr: ' + JSON.stringify(note.attr))

          Note.findByIdAndUpdate(note._id, { attr: note.attr }, { new: true })
           .then(note => {
            logger.put('note updated, attr: ' + JSON.stringify(note.attr))
          }).catch(err => {
            logger.put('ERROR: ' + err.message)
          })
        }
      }
    }
  }
  //logger.put('recognize return, found: ' + JSON.stringify(found))
  return found
}