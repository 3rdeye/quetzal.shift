const path       = require('path');
const url		     = require('url');
const http       = require('http');
const cheerio    = require('cheerio');
const request    = require('request');

const logger = require('../helpers/logger.js')


/*
 * find website title for a url
 */

exports.lookupPageTitle = (src, callback) => {
  logger.put('agent.findPageTitle: ' + src)
  
	request(src, function(error, response, html)
  {
		if(error) {
      logger.put("FAIL: " + error)
			return null
		} else {
			const $ = cheerio.load(html)
      const title = $("title").text()
			logger.put('>> title found: ' + title)
      
			callback(title)
		}
	})
}

exports.extract = function(src, ext, callback) {
	logger.put('agent.extract: ' + src)
	logger.put('extraction: ' + JSON.stringify(ext))
	
	var getAfter = function(input, term) {
		var pos = input.indexOf(term)
		if(pos == -1) return null
		return input.slice(pos + term.length, -1)
	}
  
	request(src, function(error, response, html)
  {
		if(error) {
      logger.put("FAIL: " + error)
			callback(error)
		} else {
			const $ = cheerio.load(html)
      const elem = $("*") //[content^='soundcloud://sounds:']") //$(ext.domQuery)
			
			const title = $("title").text()
			
			for(i=0; i<elem.length;i++) {
				var x = $(elem[i]).html()
				if(x.indexOf('74698236') != -1)
					logger.put('>>> ' + $(elem[i]).html())
			}
			
			
			var term = ''
			var result = ''
			
			if(ext.extractAttr)
				term = elem.attr(ext.extractAttr)
			
			if(ext.extractAfter)
				result = getAfter(term, ext.extractAfter)
				
			logger.put('>> extraction: ' + result)
      
			callback(null, result)
		}
	})
}
