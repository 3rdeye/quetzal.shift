/*

// .show:
Note.find({ '_id': { $in: c.items }}).sort({ 'rating': -1 }).then(notes => {})


// .emptyTrashDo

Cloud.update( {},{ $pullAll: { items: trashNotes } },
                 { multi: true }, function (err) {} )





// from code example:  Update a cloud identified by the cloudId in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body.content) {
    return res.status(400).send({
      message: "Cloud content can not be empty"
    });
  }

  // Find Cloud and update it with the request body
  Cloud.findByIdAndUpdate(req.params.cloudId, {
      title: req.body.title || "Untitled Cloud",
      content: req.body.content
    }, {
      new: true
    })
    .then(cloud => {
      if (!cloud) {
        return res.status(404).send({
          message: "Cloud not found with id " + req.params.cloudId
        });
      }
      res.send(cloud);
    }).catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: "Cloud not found with id " + req.params.cloudId
        });
      }
      return res.status(500).send({
        message: "Error updating Cloud with id " + req.params.cloudId
      });
    });
};


// remove

  Cloud.findById(cloudId).then(cloud => {
    if(cloud.items.length > 0) {
      return responder.errNotPossible(res,
               "Cloud with id " + cloudId + " is not empty.")
    }
    else {
      Cloud.findByIdAndRemove(cloudId)
        .then(cloud => {
            if(!cloud) {
                return responder.errNotFound(res,
                  "Cloud not find cloud with id " + cloudId)
            }
            return responder.okMsg(res,
                     "Cloud deleted successfully!");
        }).catch(err => {
          return responder.err(res, err) 
          //if(err.kind === 'ObjectId' || err.name === 'NotFound') {
          //else
          //  return responder.errres.status(500).send({
          //      message: "Could not delete Cloud with id " + cloudId
          //  });
        });
    }
  })





// show all notes

exports.allNotes = (req, res) => {
  logger.in('cloud.allNotes', req, res)
  
  Cloud.find().then(clouds => {
    Note.find().then(notes => {
      
      // collect IDs of all clouds' notes
      var idsInCloud = []
      for(var c in clouds) {
        for(var i = 0; i < clouds[c].items.length; i++) {
          idsInCloud.push(clouds[c].items[i]) } }

      // iterate over all notes to check if they are in a cloud
      for(var n in notes) {
        var note = notes[n]
        
        var isInArray = function (id) {
          for(var ii = 0; ii < idsInCloud.length; ii++) {
            if(idsInCloud[ii].equals(id)) return true
          } return false }

        // if note id appears in array of notes, mark as clouded
        if(isInArray(note._id))
          note.clouded = true
      }
      
      responder.ok(res, 'index', {
        vars: {
          clouds: clouds,
          notes: notes },
        view: 'allNotes' }
      ) }) }) }



// Helper to remove all notes from clouds

exports._removeNotes = (req, res) => {
  logger.in('cloud.removeNotes', req, res)
  Cloud.update({}, { $set: { items: [] }}, {multi: true}, function(err, affected) {
    if(err) {
      logger.out("[ERR] " + err.message)
      res.json(500, { message: "Could not remove items from clouds" })
    }
    else {
      logger.out('affected ' + affected)
      res.json(affected)
    }
  })
}





*/