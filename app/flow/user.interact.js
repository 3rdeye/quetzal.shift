const responder = require('./helpers/responder.js')
const logger    = require('./helpers/logger.js')

const User  = require('../models/user.model.js')


// -=-=-=-=-=-=- //


// Create and Save a new Note
exports.create = (req, res) => {
  logger.in('user.create', req, res)
  
    // validate parameters
  if (!responder.validate(res, { 'email': req.body.email })) return
  if (!responder.validate(res, { 'username': req.body.username })) return
  if (!responder.validate(res, { 'password': req.body.password })) return
  if (!responder.validate(res, { 'passwordConf': req.body.passwordConf })) return
  
    // check password confirmation
  if (req.body.password != req.body.passwordConf)
    return responder.err(res, { message: 'password is not equal to confirmation' })
  
  var userData = {
      email: req.body.email,
      username: req.body.username,
      password: req.body.password,
  }
    //use schema.create to insert data into the db
  User.create(userData, function (err, user) {
    if (err) {
      return responder.err(res, err)
    } else {
      return responder.okMsg(res, 'new user created.')
    }
  })
}

exports.login = (req, res) => {
  logger.in('user.login', res, req)
  
  if (req.method == "GET") {
    if(req.user)
      return responder.okRedirect(res, '/clouds')
    else
      return responder.ok(res, './user/login')
  }
  else if (req.method == "POST") {
    var user = req.body.user
    var pwd = req.body.pwd

    if (!responder.validate(res, { 'user': user, 'pwd': pwd })) return

    User.authenticate(user, pwd, function(err, u) {
      if(err)
        responder.okMsg(res, err.message)
      else if(!u)
        responder.okMsg(res, 'wrong user-password combo')
      else {
        req.session.userId = u._id;
        //logger.put('logged in as user: ' + JSON.stringify(u))
        responder.okMsg(res, 'logged in as user: ' + u.username)
      }
    })
  }
}

exports.logout = (req, res) => {
  logger.in('user.logout', res, req)

  if (req.session) {
    // delete session object
    req.session.destroy(function(err) {
      if(err) {
        return responder.err(res, err)
      } else {
        return responder.okMsg(res, 'you are logged out')
      }
    })
  }
  else
    responder.err(res, { message: 'no user logged in' })
}