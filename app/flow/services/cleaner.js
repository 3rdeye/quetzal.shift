const responder = require('../helpers/responder.js')
const logger    = require('../helpers/logger.js')

const Cloud = require('../../models/cloud.model.js')
const Note  = require('../../models/note.model.js')

// -=-=-=-

// empty SACRIFICE cloud, will point to confirmation dialog

var trashCloudName = 'TRASH'
var trashNotes = null

exports.cleanup = (req, res) => {
  logger.in('cloud.cleanup', req, res)
  
  if (req.method == 'GET')
  {
    Cloud.findOne({ name: trashCloudName }).then(c => {
      logger.put('trash cloud: ' + JSON.stringify(c))
      trashNotes = c.items

      // find all notes in trash and put out to user to confirm
      Note.find({ '_id': { $in: trashNotes }}).then(notes => {
        logger.put('notes in trash: ' + JSON.stringify(notes))
        return responder.ok(res, 'confirm', {
          vars: { notes: notes },
          pagetitle: 'confirm cleanup',
          question: 'Quieres borrar estas notas?',
          route: '/cleanup'
        })
      }).catch(err => {
        return responder.err(res, err)
      })
    })
  }
  else if(req.method == 'POST')
  {
    if(!responder.validate(res, { 'confirm': req.body.confirm })) return
    
    if(req.body.confirm != 'yes') {
      logger.put('cleanup CANCELED')
      return responder.okRedirect(res, '/')
    }
    if(!trashNotes) {
      return responder.errNotPossible(res, 'no notes to throw away')
    }

    Note.find({ '_id': { $in: trashNotes }}).remove().then(notes => {
      logger.out('notes removed: ' + JSON.stringify(notes))

      // remove references

      Cloud.update( { name:trashCloudName },
                    { items: [], nItems: 0 }).then(() => {

          trashNotes = null
          logger.put('references removed')

          return responder.okRedirect(res, '/')

      }).catch(err => { return responder.err(res, err) })
    }).catch(err => { return responder.err(res, err) }) 
  }
}