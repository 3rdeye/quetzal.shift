const responder = require('../helpers/responder.js')
const logger    = require('../helpers/logger.js')

const patterns  = require('../wisdom/patterns.js')
const agent  = require('../agents/web.agent.js')

const Cloud = require('../../models/cloud.model.js')
const Note  = require('../../models/note.model.js')

/*
 * search for dead oId references
 */

//////////////// do by promises? ///// /// // / /  /   / 

/*
 * QUICK HACKS
 */
exports.quickHack = (req, res) => {
  logger.in('healer.quickHack', req, res)
	
	var regEx = /(\d+)/g
	var src = 'https://soundcloud.com/lucasbardaman/roots-dub-reggae-mix-12-1-13'
	var ext = patterns.getExtract('url-soundcloud')
	
	agent.extract(src, ext, (err, id) => {
		if(err) return responder.err(res, err)
		return responder.okMsg(res, 'ID: ' + id)
	})
	
	//var url = 'soundcloud://sounds:74698236'
	//var id = getAfter(url, 'sounds:') //url.match(new RegExp(regEx))[0]
}



						
exports.findDeadRefs = (req, res) => {
  logger.in('healer.findDeadRefs', req, res)
  
  var deadRefCount = 0
  
  Cloud.find().then(clouds => {
    // iterate all clouds
    for(c = 0; c < clouds.length; c++) {
      (function() {
        var cloud = clouds[c]
        // iterate all items in that cloud
        for(n = 0; n < clouds[c].items.length; n++) {
          (function() {
            
            var note = clouds[c].items[n]
            var noteId = note._id

            // fetch actual note
            Note.find({_id: noteId}).then(notes => {
              // no note found? ->
              if(notes.length === 0) {
                // we have a dead reference
                logger.put('[' + (++deadRefCount) + '] found dead reference in cloud "'+
                           cloud.name + '" to id: ' + noteId)
                //logger.put(JSON.stringify(note))
                
                /* REMOVE dead refs */
                Cloud.update({ _id: cloud._id }, {
                  $pull: { "items": noteId }}, function(err) {
                    if(err) {
                      return responder.err(res, err)
                    } else {
                      logger.put('cleaned dead reference to note, id: ' + noteId)
                    }
                })
                /**/
              }
            })
          })()
        }
      })()
    }
    
    return responder.okMsg(res, 'dead references will be cleaned...') 
  })
  
  /* stackoverflow code
  Cloud.findById(cloudId)
    .populate('items')
    .exec((err, cloud) => {
        cloud.items = cloud.items.filter(item => item != null);

        res.send(cloud); // Return result as soon as you can
        cloud.save(); // Save cloud without dead refs to database
    })*/
}


/*
 * HELPER method to set cloud.nItems
 */

exports.updateItemCounts = (req, res) => {
  logger.in('healer.updateItemCounts', req, res)
  
  var nUpdated = 0
  
  Cloud.find().then(clouds => {
    clouds.forEach(cloud => {
      var nBefore = cloud.nItems

      if(nBefore != cloud.items.length) {
        cloud.nItems = cloud.items.length

        logger.put('updated [' +cloud.name+ '] ' +nBefore+ ' -> ' + cloud.nItems)

        cloud.save()
        nUpdated++
      }
    })
    // NEED TO implement PROMISES
  })
  
  return responder.okMsg(res, 'clouds will be updated.')
}

/*
 * run recognition for all notes
 */

exports.recognizeAll = (req, res) => {
  logger.in('healer.recognizeAll', req, res)
  
  Note.find().then(notes => {
    var found = []
      // iterate over all notes
    for(i = 0; i < notes.length; i++) {
        // get note content
      var content = notes[i].content
        // check for recognize patterns anbd update notes
      var result = recognize(notes[i])
        // push to results if not empty
      if(Object.keys(result) > 0)
        found.push(result)
    }
    
    logger.put('note recognition done. notes updated: ' + found.length)
    
    return responder.okMsg(res,
      'recognizeAll done. found:<br><br>' +JSON.stringify ( found ))
  })
}


/*
 * find youtube titles for all notes
 */

exports.findYoutubeTitles = (req, res) => {
  logger.in('healer.findYoutubeTitles', req, res)
  
  Note.find({ 'attr.url-youtube': { $exists: true } }).then(notes => {
    logger.put('yt notes found: ' + notes.length)
    for(i = 0; i < notes.length; i++) {
      (() => {
        var note = notes[i]
        setTimeout(() => {
          //logger.put('find title for: ' + note.content)
          agent.lookupPageTitle(note.content, function(title) {
            note.title = title
						note.save()
						  .then(n => { logger.put('note updated, id ' + n.id) })
						  .catch(err => responder.err(res, err))
          })
        }, 500 * i + getRandomInt(0, 1000))
      })()
    }
  })
  
  logger.out('done.')
  responder.okMsg(res, 'updating youtube titles ...')
}



