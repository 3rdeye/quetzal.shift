const responder = require('../helpers/responder.js')

const User  = require('../../models/user.model.js')

/*
 * require login for a route
 */
exports.requiresLogin = function(req, res, next) {
  console.log('URL >> ' + req.url)
  if (req.url === '/login' ||
      req.url === '/pagecount' ||
      req.session && req.session.userId && req.user &&
      req.session.userId == req.user._id) {
    return next()
  } else {
    //var err = new Error('You must be logged in to view this page.')
    //err.status = 401
    //return next(err)
    return responder.okMsg(res, 'You must be logged in to go here.', 'login', 'login')
  }
}

/*
 * check if user is logged in and populate locals if so
 */

exports.checkUser = function(req, res, next) {
    if(req.session)
      console.log('>>> ' + JSON.stringify(req.session))
    //next()
    if (req.session && req.session.userId) {
      User.findById( req.session.userId , function(err, user) {
        if (user) {
          var userObj = {
            _id: user._id,
            username: user.username,
            email: user.email
          }
          req.user = userObj
          //req.session.user = userObj  //refresh the session value
          res.locals.user = userObj;
        }
        // finishing processing the middleware and run the route
        next();
      });
    } else {
      next();
    }
}