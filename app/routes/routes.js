module.exports = (app) => {
  const notes  = require('../flow/note.interact.js');
  const clouds = require('../flow/cloud.interact.js');
  const users = require('../flow/user.interact.js');
  
  const auth = require('../flow/midware/auth.js');

  const cleaner = require('../flow/services/cleaner.js')
  const healer = require('../flow/services/healer.js')
  

  /**** ROUTES ****/
  
  // 2do: add parameters!
  //
  // like: app.get('/notes/:noteId', notes.findOne);
  
  
  app.use(auth.checkUser)     // always check if user is logged in
  app.use(auth.requiresLogin) // always require login for all routes

  /*
   * Users
   */

  // [Create] & [Login/out]
  app.post('/users/create', users.create) // create a new user
  app.get( '/login',  users.login)   // request login
  app.post('/login',  users.login)   // login user
  app.get( '/logout', users.logout)   // logout user


  /*
   * Clouds
   */

  // [Create] & [Delete]
  app.post('/clouds/create', clouds.create) // create a new cloud
  app.post('/clouds/remove', clouds.remove) // remove a cloud by id

  // [Read]
  app.get('/',       users.login)
  app.get('/clouds', clouds.overview) // show all clouds and their notes

  app.get('/show', clouds.show) // retrieve all clouds and notes of [cloudId]

  // [Update]
  app.post('/move', clouds.move) // move notes to cloud
  app.post('/setCloudColor', clouds.setColor) // set color of a cloud


  /*
   * Notes
   */

  // [Create]
  app.post('/notes/create', notes.create) // create a new note
  
  // [Update]
  app.post('/notes/vote', notes.vote)     // vote note up / down

  /*
   * HELPERS
   */
  
  // empty trash cloud
  app.get('/cleanup',  cleaner.cleanup)
  app.post('/cleanup', cleaner.cleanup)
  
  app.get('/hack', healer.quickHack)

  /*/ print out all notes as text
  app.get('/print', notes.print)

  // recognize all notes
  app.get('/recognize', notes.recognizeAll)
  app.get('/findyttitles', agent.findYoutubeTitles)
  // find dead references
  app.get('/deadrefs', clouds.findDeadRefs)
  // find dead references
  app.get('/updateitemcounts', clouds.updateItemCounts)
  */
  
  // route for handling 404 requests(unavailable routes)
  //app.use(function (req, res, next) {
  //  res.status(404).send("Sorry can't find that!<br><a href='/'>back</a>")
  //})
}