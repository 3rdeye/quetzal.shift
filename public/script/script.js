function isMobile() {
  if( navigator.userAgent.match(/Android/i) ||
    navigator.userAgent.match(/webOS/i) ||
    navigator.userAgent.match(/iPhone/i) ||
    navigator.userAgent.match(/iPad/i) ||
    navigator.userAgent.match(/iPod/i) ||
    navigator.userAgent.match(/BlackBerry/i) ||
    navigator.userAgent.match(/Windows Phone/i)
 ){ return true; } else { return false; }
}

var userOnMobile = isMobile()

// mobile screen orientation check //

var previousOrientation = window.orientation;

var orientate = function() {
    //alert('w: ' +screen.width+ ', h: ' +screen.height)
  var width = userOnMobile ? screen.width : window.innerWidth
  
  if(width > 600) {
      $('body').addClass('size600')
    } else {
      $('body').removeClass('size600')
    }
}

var checkOrientation = function(){
    if(window.orientation !== previousOrientation){
        previousOrientation = window.orientation
        orientate()
    }
}


// (optional) Android doesn't always fire orientationChange on 180 degree turns
if(userOnMobile)
  setInterval(checkOrientation, 2000);

// -=-=-=-=- //

// day / night toggle
var toggleDayNight = () => {
  $('body').toggleClass('night')
}

// sort clouds

var sortClouds = (by) => {
  var eBtn = $('.allClouds .sort a[sortBy="'+by+'"]')
  
  var order = eBtn.attr('order') == 'up' ? 'up' : 'down'
  var iOrder = (order == 'up') ? 1 : -1
  var attr = 'data-' + by
  console.log('sort clouds by: ' + attr + ', ' + order)
  
  $('.allClouds .cloud').sort(function (a, b) {
      var contentA =parseInt( $(a).attr(attr))
      var contentB =parseInt( $(b).attr(attr))
      return (contentA < contentB) ? iOrder : (contentA > contentB) ? (-iOrder) : 0;
  }).appendTo($('.allClouds'))
  
  if(order == 'up') {
    eBtn.attr('order', 'down')
    eBtn.find('span').html('&#9660') //&#8613;')
  } else {
    eBtn.attr('order', 'up')
    eBtn.find('span').html('&#9650') //&#8615;')
  }
}

// open / close clouds

var openCloseCloud = (id) => {
  console.log('openCloseCloud('+id+')')
  var eCloud = $('.cloud[cloudId=' +id+ ']')
  var eNotes = eCloud.find('.notes')
  if(eNotes.hasClass('closed')) {
    eNotes.show()
    eNotes.removeClass('closed')
  } else {
    eNotes.hide()
    eNotes.addClass('closed')
  }
}

var openAllClouds = () => {
  console.log('openAllClouds()')
  $('.allClouds .cloud').each((i,e) => {
    var eNotes = $(e).find('.notes')
    if(eNotes.hasClass('closed')) {
      eNotes.show()
      eNotes.removeClass('closed')
    }
  })
}
var closeAllClouds = () => {
  console.log('closeAllClouds()')
  $('.allClouds .cloud').each((i,e) => {
    var eNotes = $(e).find('.notes')
    if(!eNotes.hasClass('closed')) {
      eNotes.hide()
      eNotes.addClass('closed')
    }
  })
}

/*
 * create a color picker to set cloud's color
 */

var createColorPicker = () => {
    // get element and cloudId
  var elem = $(".colorPicker-wrap")
  var cloudId = elem.attr('data-cloudId')
  
    // update link to toggle
  var eLink = elem.prev('.toggle-colorPicker')
  eLink.attr('onclick', '$(".colorPicker-wrap").toggle()')
  
    // create colorpicker elements
  var size = 200; var n = 10; var d = size / n
  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      var colR = parseInt(255 * i/n)
      var colG = parseInt(255 * j/n)
      var colB = parseInt(155 - (i/n * 155))
      var color = 'rgb(' +colR+ ',' +colG+ ',' +colB+ ')'
      var colorStyle =
         'width:' +d+ 'px; height: ' +d+ 'px;' +
         'left:' + i*d + 'px;' +
         'top:' + j*d + 'px;' +
         'background: ' + color

      var eColor = $(
        '<form action="setCloudColor?cloudId=' +cloudId+ '" method="post">' +
          '<input type="hidden" name="color" value="' +color+ '" /> ' +
          '<a href="javascript:;" onclick="parentNode.submit();" class="color" style="' +colorStyle+ '"></a>' +
        '</form>')
      
      eColor.appendTo(elem)
    }
  }
  elem.show()
}

/* -=-=-=- YOUTUBE VIDEOS -=-=-=- */

var ytApiRequested = false  // is yt api ready?
var ytPlayer = null         // yt player object
var ytPlayIdOnLoad = null   // note id we are gonna play once ready
//var ytIsOpen = false      // currently displaying a video?

function onYouTubeIframeAPIReady() {
  console.log('YT API ready')
  if(ytPlayIdOnLoad) {
    playVideo(ytPlayIdOnLoad)
    ytPlayIdOnLoad = null
  }
}

var loadYtApi = () => {
  var tag = document.createElement('script');
  tag.src = "https://www.youtube.com/iframe_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  console.log('YT-API requested')
  ytApiRequested = true
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
  console.log('YT onPlayerReady()')
  
  //////////
  //ytPlayer.setPlaybackRate(1.25);
  
  //event.target.playVideo();
}

function onPlayerStateChange(event) {
  console.log('YT Player event: ' + event.data)
  //if (event.data == YT.PlayerState.PLAYING && !done) {
    //setTimeout(stopVideo, 6000);
    //done = true;
  //}
}
function stopVideo() {
  ytPlayer.stopVideo();
}


var playVideo = (noteId) => {
  if(ytApiRequested == false) {
    ytPlayIdOnLoad = noteId
    return loadYtApi(noteId)
  }
  
  var eNote = $('.note[noteId="' +noteId+ '"]')
  var eLink = eNote.find('.play-video')
  var url = eLink.attr('url')
  
  var isPlaying = eLink.hasClass('playing')
  
  // if player is open, remove it and change link button
  if($('#yt-player').length > 0) {
    console.log('close YT player')
    
    var ePlayer = $('#yt-player');
    var e = ePlayer.parent().find('.play-video')
    
    stopVideo()
    ePlayer.remove()
    e.html('play')
    e.removeClass('playing')
    
    if(isPlaying)
      return
  }
  
  var isYoutube = url && url.match(/(?:youtu|youtube)(?:\.com|\.be)\/([\w\W]+)/i)
  if (isYoutube) {
    id = isYoutube[1].match(/watch\?v=|[\w\W]+/gi)
    id = (id.length > 1) ? id.splice(1) : id
    id = id.toString();
    console.log('Youtube ID: ' + id)
    
    var eVideo = $('<div><div id="yt-player" ytId="' +id+ '"></div></div>')
    eVideo.appendTo(eNote)
    
    /*/ append slider
    var eSlider = $(
      '<div class="slider" func="changePlaybackRate">'+
        '<div class="slider-btn">'+
        '</div>'+
      '</div>'
    )
    eNote.append(eSlider)
    initSlider(eSlider, (val) => {
      var rate = 0.5 + val * 1.5
      ytPlayer.setPlaybackRate(rate)
      //console.log('set playback rate: ' + rate)
    })*/
    
    eLink.html('stop')
    eLink.addClass('playing')
    
    
    ytPlayer = new YT.Player('yt-player', {
      height: '180',
      width: '320',
      videoId: id,
      events: {
        'onReady': onPlayerReady,
        'onStateChange': onPlayerStateChange
      }
    })
  }
  else {
    // display error
    console.log('ERROR: url not recognized for youtube: ' + url)
    eNote.append($('<div style="color:red">URL not recognized!</div>'))
  }
}
  

/* -=-=-=- YOUTUBE END -=-=-=- */

/* -=-=-=- DOCUMENT READY -=-=-=- */

$(document).ready(function() {
  if(userOnMobile) {
    window.addEventListener("resize", checkOrientation, false);
    window.addEventListener("orientationchange", checkOrientation, false);
  } else {
    window.addEventListener("resize", orientate, false);
  }

  orientate()
  
  // clear fresh input elements on focus
  
  $('.fresh').on("focus keydown paste", function(e) {
    if($(this).hasClass('fresh')) {
        $(this).removeClass('fresh')
        $(this).text('')
        $(this).val('')

        $(this).closest("form").find('.btn:input[type="submit"]')
          .prop('disabled', false)
    }
  })
  
  // note selection

  $('.note').click(function() {
    var eNote = $(this);
    // note is selected? -> unselect
    if( $(this).hasClass('selected') ) {

      // unselect element
      $(this).removeClass('selected');

      // remove hidden field from cloud link buttons
      $('#move-to-cloud form input[value=' +eNote.attr('noteId')+ ']').remove()

      // no more notes selected? -> switch to see-clouds
      if($('.note.selected').length === 0) {
        $('#see-clouds').show();
        $('#move-to-cloud').hide();
      }
    } else {

      // select one note

      if($('.note.selected').length === 0) {
        $('#see-clouds').hide();
        $('#move-to-cloud').show();
      }
      $(this).addClass('selected');

      // iterate over buttons' forms and add fields

      $('#move-to-cloud form').each(function(i,e)
      {
        var eField = $('<input type="hidden"' +
                         'name="noteIds[]" ' +
                         'value="' + eNote.attr('noteId') +
                       '">')

        $(this).prepend( eField )

        //console.log('>>> ' + i)
        //console.log('>>> ' + $(this).html())
      })
    }

    // are notes selected?
    if($('.note.selected').length > 0) {
        $('#see-clouds').hide();
        $('#move-to-cloud').show();
    } else {
      $('.clouds .cloud').each(function(i,e) {
        $('#see-clouds').show();
        $('#move-to-cloud').hide();
      })
    }
  }) // end - .note.click
  
  // prevent selection when link triggered
  
  $(".note a").click(function(e) {
        e.stopPropagation();
  });
  
  // sliders
  
  //$('.slider').each((i,e) => {
    /////////////////////////////
  //}) // end - .slider.each
})

var initSlider = (elem, callback) => {
  var btn = elem.find('.slider-btn')
  
  elem.click((e) => {
    e.stopPropagation()
  })
    
  btn.mousedown((e) => {
    var left = parseInt(btn.css('left'))
    console.log('mousedown, x: ' + e.pageX)
    console.log('btn position left: ' + left)
    
    btn.addClass('drag')
    btn.attr('dragStartUser', e.pageX)
    btn.attr('dragStartElem', left)
  })
  
  
  elem.mousemove((e) => {
    if(btn.hasClass('drag')) {
      var xDif = parseInt(e.pageX - btn.attr('dragStartUser'))
      var xPos = parseInt(btn.attr('dragStartElem')) + xDif
      
      // check if element is inside it's borders
      var xMax = elem.width() - btn.width()
      if(xPos < 0) {xPos = 0}
      else if(xPos > xMax) {xPos = xMax}
      
      // percentage
      var value = xPos / xMax
      
      btn.css('left', xPos + 'px')
      //console.log('mousemove, xDif: ' +xDif+ ', xPos: ' +xPos+ ', value: ' + value)
      callback(value)
    }
  }).mouseout((e) => {
    console.log('mouseout')
    btn.removeClass('drag')
    //btn.removeAttr('dragStart')
  })
  .mouseup((e) => {
    console.log('mouseup')
    btn.removeClass('drag')
    //btn.removeAttr('dragStart')
  });
}