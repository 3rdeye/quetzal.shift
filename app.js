const express      = require('express');
const bodyParser   = require('body-parser');
const cookieParser = require('cookie-parser');
const session      = require('express-session');
const path         = require('path');
const moment       = require('moment-timezone');       // date & time
const hashish      = require('./app/flow/helpers/hashish.js') // hashing

const app		= express();

const port	= process.env.PORT || 3000; // 443


  // parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
  // parse requests of content-type - application/json
app.use(bodyParser.json());
app.use(cookieParser());

app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));  // public folder
app.use('/favicon.ico', express.static(__dirname + '/public/images/quetzal.ico'));

/*
 * LOGGING
 */

var myLogger = function (req, res, next) {
  var zone = 'America/Mexico_city';
  console.log('\n[' +
              moment().tz(zone).format('HH:mm:ss') +
              '] [REQUEST] ' + req.url);
  next();
};

app.use(myLogger);

/*
 * SESSION
 */

app.use(session({
	key: 'userId',
	secret: hashish.salt(),
	resave: false,
	saveUninitialized: false,
	cookie: {
			expires: 24 * 60 * 60 * 1000
	}
}))


/*
 * DATABASE
 */

  // Confige the database
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

  // Connect to the database
mongoose.connect(dbConfig.url)
	.then(() => {
		console.log("Successfully connected to the database")

		/*
		 * connect our app
		 */
		app.listen(port, function() {
			console.log('Quetzal volando en puerto ' + port + ' ...')

			//dbTest.runTests(db);
		})
	}).catch(err => {
			console.log('Could not connect to the database. Exiting now...')
    process.exit()
});

/*
 * ROUTES
 */

require('./app/routes/routes.js')(app)
